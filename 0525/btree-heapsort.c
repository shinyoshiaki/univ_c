#include <stdio.h>

static void heapify(int a[], int size, int hole, int new_element);

void sort_method(char *method_name)
{
    sprintf(method_name, "Heapsort");
}

void sort(int a[], int size)
{
    int k, tmp;
    for (k = size / 2 - 1; k >= 0; --k)
        heapify(a, size, k, a[k]);
    for (k = size - 1; k >= 1; --k)
    {
        tmp = a[k];
        a[k] = a[0];
        heapify(a, k, 0, tmp);
    }
}

static void heapify(int a[], int tree_size, int hole, int new_element)
{
    int siftup_cand;
    /* siftup candidate */
    while ((siftup_cand = hole * 2 + 1) < tree_size)
    {
        if (siftup_cand + 1 < tree_size
            /* 右の子も居て */
            && a[siftup_cand] < a[siftup_cand + 1])
            /* 右の子の方が */
            ++siftup_cand;
        /* 大きい場合は */
        /* 右の子が siftup の候補 */
        if (new_element >= a[siftup_cand])
            break;
        /* new_element を hole の場所に入れれば良い */
        a[hole] = a[siftup_cand];
        hole = siftup_cand;
    }
    a[hole] = new_element;
}