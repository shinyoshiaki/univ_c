/**************************************************************/
/* Quicksort  : 再帰計算の例                                  */
/*------------------------------------------                  */
/*    大きさ100の配列にランダムに整数を生成し、その配列要素を */
/*    Quicksortアルゴリズムを使って昇順に並べ替えて出力する。 */
/**************************************************************/

#include <stdio.h>
#include <stdlib.h>     /* 乱数発生のライブラリ関数を使うため */

#define  SIZE   100
#define  WIDTH  10
#define  TRUE    1

void set_an_array_random(int x[], int size);
void pretty_print(int x[], int size);
void quicksort(int x[], int from, int to);
int  partition(int x[], int from, int to);

int main(void)
{
  int  a[SIZE], seed;    /* a[SIZE] を外部配列にはしない */

  printf("Input a random seed (0 - %d):  ", RAND_MAX);
  scanf("%d", &seed);
  srand(seed);

  set_an_array_random(a, SIZE);
  printf("\nbefore sorting:\n");
  pretty_print(a, SIZE);

  quicksort(a, 0, SIZE-1);
  printf("\nafter sorting:\n");
  pretty_print(a, SIZE);
  return 0;
}

/*------------------------------------------------------------*/
/* 引数で与えられた配列の各要素をランダムに設定する           */
/*------------------------------------------                  */
/* (仮引数) x    : int型配列                                  */
/*          size : int型配列 x の大きさ                       */
/* (機能) :  配列要素 x[0]〜x[size-1] に 0〜999 の間の乱数を  */
/*           設定する。                                       */
/*------------------------------------------------------------*/
void set_an_array_random(int x[], int size)
{
  int i;

  for (i=0; i<size; ++i)
    x[i] = rand() % 1000;
}

/*----------------------------------------------------------*/
/* 引数で与えられた配列の要素を順番に全て出力する           */
/*------------------------------------------                */
/* (仮引数) x    : int型配列                                */
/*          size : int型配列 x の大きさ                     */
/* (機能) :  配列要素 x[0]〜x[size-1] の値を順番に全て出力  */
/*           する。但し、各々の値は横幅7カラムのフィールド  */
/*           に出力することにし、また、1行にWIDTH個の要素   */
/*           を出力する。                                   */
/*----------------------------------------------------------*/
void pretty_print(int x[], int size)
{
  int i, count=1;

  for (i=0; i<size; ++i, ++count) {
    printf("%7d", x[i]);
    if (count >= WIDTH) {
      printf("\n");
      count = 0;
    }
  }
  if (count > 1)
    printf("\n");
}

/*----------------------------------------------------------*/
/* 引数で与えられた配列要素を小さい順に並べ替える           */
/*------------------------------------------                */
/* (仮引数) x    : int型配列                                */
/*          from : int型配列 x の添字                       */
/*          to   : int型配列 x の添字                       */
/* (関数値)  :  なし                                        */
/* (機能) :  quicksortアルゴリズムを使って、配列要素        */
/*                x[from],x[from+1],x[from+2], ..., x[to]   */
/*           を値の小さい順に並べ替える。                   */
/*----------------------------------------------------------*/
void quicksort(int x[], int from, int to)
{
  int  pivot_sub;              /* pivot subscript の意 */

  if (from < to) {
    pivot_sub = partition(x, from, to);       /* 分割操作 */
    quicksort(x, from, pivot_sub - 1);
    quicksort(x, pivot_sub + 1, to);
  }
}

/*----------------------------------------------------------*/
/* 引数で与えられた配列の部分列にquicksortの分割操作を施す  */
/*                                        (quicksortの関数) */
/*------------------------------------------                */
/* (仮引数) x    : int型配列                                */
/*          from : int型配列 x の添字                       */
/*          to   : int型配列 x の添字                       */
/* (関数値)  :  分割操作によって得られた枢軸要素の添字番号  */
/*              (以下の「(機能)」の項で出て来る pivot_sub)  */
/* (機能) :  x[from]〜x[to]を並べ替えて                     */
/*           max{x[from],...,x[pivot_sub-1]} <= x[pivot_sub]*/
/*           x[pivot_sub] < min{x[pivot_sub+1],...,x[to]}   */
/*           となるようにする。                             */
/*----------------------------------------------------------*/
int  partition(int x[], int from, int to)
{
  int pivot;

  pivot = x[from];     /* 最初の要素を枢軸要素に選ぶ。 */
  while (TRUE) {       /* 工夫の余地あり。             */
    for ( ; from<to && x[to]>pivot; --to)
      ;
    if (from == to) {
      x[from] = pivot;
      return from;
    }
    x[from++] = x[to];

    for ( ; from<to && x[from]<=pivot; ++from)
      ;
    if (from == to) {
      x[to] = pivot;
      return to;
    }
    x[to--] = x[from];
  }
}
