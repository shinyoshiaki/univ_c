/************************************************************/
/* Btree-Traversesortモジュール                                     */
/*------------------------------------------                */
/*    外部へのサービスを行うために、次の２つの関数がこの    */
/*    モジュールの中に用意されている。                      */
/*    (1)整列化アルゴリズムの名前を答える関数 sort_method,  */
/*    (2)配列要素をBtree-Traversesortアルゴリズムで                 */
/*                   小さい順に並べ替える関数 sort          */
/************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define MAXLENGTH 100000000
typedef struct btree_item *Btree;

typedef struct btree_item
{
    int num;
    Btree left_child, right_child;
} Btree_item;

static Btree add_item(Btree tree, int num);
static void inorder_traverse(Btree ptr);

static int sorted[MAXLENGTH], sorted_pos = 0;

/*----------------------------------------------------------*/
/* 整列化アルゴリズムの名前を答える                         */
/*------------------------------------------                */
/* (引数) method_name : 出力用。このchar型配列に方式の名前  */
/*                      を(文字列として)入れて返す。配列の  */
/*                      大きさは 11 文字分必要。            */
/*----------------------------------------------------------*/
void sort_method(char *method_name)
{
    sprintf(method_name, "Traversesort");
}

/*----------------------------------------------------------*/
/* 配列要素を小さい順に並べ替える ( Btree-Traversesort)             */
/*------------------------------------------                */
/* (仮引数) a    : int型配列                                */
/*          size : int型配列 a の大きさ                     */
/* (関数値)  :  なし                                        */
/* (機能) :  Btree-Traversesortアルゴリズムを使って、配列要素       */
/*                a[0],a[1],a[2], ..., a[size-1]            */
/*           を値の小さい順に並べ替える。                   */
/*----------------------------------------------------------*/
void sort(int a[], int size)
{
    Btree root = NULL;
    int i;
    for (i = 0; i < size; i++)
    {
        root = add_item(root, a[i]);
    }
    inorder_traverse(root);

    for (i = 0; i < size; i++)
        a[i] = sorted[i];
}

static Btree add_item(Btree tree, int num)
{
    Btree_item *new_item;

    if (tree == NULL)
    {
        new_item = (Btree_item *)malloc(sizeof(Btree_item));
        if (new_item == NULL)
        {
            printf("*** fail in memory allocation ***");
        }

        new_item->num = num;
        new_item->left_child = NULL;
        new_item->right_child = NULL;
        return new_item;
    }
    else if (num < tree->num)
    {
        tree->left_child = add_item(tree->left_child, num);
        return tree;
    }
    else
    {
        tree->right_child = add_item(tree->right_child, num);
        return tree;
    }
}

static void inorder_traverse(Btree ptr)
{
    int num;
    if (ptr != NULL)
    {
        inorder_traverse(ptr->left_child);
        sorted[sorted_pos++] = ptr->num;
        inorder_traverse(ptr->right_child);
        free(ptr);
    }
}