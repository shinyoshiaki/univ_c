/************************************************************/
/* 要素数が 5, 10, 25, 50, 100, 200 の場合について、        */
/* 整列化プログラムの平均実行時間を計る                     */
/*------------------------------------------                */
/*   要素数が 5, 10, 25, 50, 100, 200 の場合について、それ  */
/*   ぞれ 800000回, 400000回, 160000回, 80000回, 40000回,   */
/*   20000回 次の作業を繰り返して所用時間を計り、後で1回当り */
/*   の計算時間を割り出す。                                 */
/*      ｜配列上に要素数分だけランダムに整数を生成し、      */
/*      ｜その配列要素を別途用意された整列化プログラムを    */
/*      ｜使って昇順に並べ替える。                          */
/************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define  MAX_SIZE  200
#define  PROB_NUM  6

typedef struct {
   int  size;
   int  ite_num;
} Problem;

typedef struct {
   double  process_time;
   double  real_time;
} Second;

void   start_timekeeper(void);
Second consumed_time(void);
void set_an_array_random(int a[], int size);
void sort_method(char *method_name);
void sort(int a[], int size);

int main(void)
{
  int     a[MAX_SIZE], seed, i, k;
  char    sort_name[100];
  Problem prob[PROB_NUM] = {{5,800000}, {10,400000}, {25,160000},
                            {50,80000}, {100,40000}, {200,20000}};
  Second  time_for_sort[PROB_NUM],
          time_for_init[PROB_NUM];

  sort_method(sort_name);
  printf("Clocking the average execution time of the program\n"
         "that sorts 5, 10, 25, 50, 100, or 200 elements.\n"
         "  (*** %s ***)\n"
         "Input a random seed (0 - %d):  ",
         sort_name, RAND_MAX);
  scanf("%d", &seed);

  /* ソート以外の部分の計算時間を測定 */
  srand(seed);
  for (k=0; k<PROB_NUM; ++k) {
    start_timekeeper();
    for (i=0; i<prob[k].ite_num; ++i) {     /* この部分の */
      set_an_array_random(a, prob[k].size); /* 計算時間を */
    }                                /* 参考のために測る。*/
    time_for_init[k] = consumed_time();
  }

  /* ソートプログラムの平均計算時間を測定 */
  srand(seed);
  for (k=0; k<PROB_NUM; ++k) {
    start_timekeeper();
    for (i=0; i<prob[k].ite_num; ++i) {     /* この部分の */
      set_an_array_random(a, prob[k].size); /* 計算時間を */
      sort(a, prob[k].size);                /* 測る。     */
    }                                       /*            */
    time_for_sort[k] = consumed_time();
                /* 次にsort部分だけの計算時間を割り出す。 */
    time_for_sort[k].process_time -= time_for_init[k].process_time;
    time_for_sort[k].real_time    -= time_for_init[k].real_time;
  }

  /* 測定結果の出力 */
  printf("\n"
         "        ** time for sort **    **time for initialize**\n"
         "size    process_t  real_time    process_t  real_time\n"
         "         (m sec)    (m sec)      (m sec)    (m sec)\n"
         "----    ---------  ---------    ---------  ---------\n");
  for (k=0; k<PROB_NUM; ++k)
    printf("%4d  %11.5f%11.5f  %11.5f%11.5f\n",
           prob[k].size,
           time_for_sort[k].process_time*1000.0/prob[k].ite_num,
           time_for_sort[k].real_time*1000.0/prob[k].ite_num,
           time_for_init[k].process_time*1000.0/prob[k].ite_num,
           time_for_init[k].real_time*1000.0/prob[k].ite_num);
  return 0;
}

/*----------------------------------------------------------*/
/* 引数で与えられた配列の各要素をランダムに設定する         */
/*------------------------------------------                */
/* (仮引数) a    : int型配列                                */
/*          size : int型配列 a の大きさ                     */
/* (関数値)  :  なし                                        */
/* (機能) :  配列要素 a[0]〜a[size-1] に 0〜999 の間の乱数  */
/*           を設定する。                                   */
/*----------------------------------------------------------*/
void set_an_array_random(int a[], int size)
{
  int i;

  for (i=0; i<size; ++i)
    a[i] = rand() % 1000;
}
