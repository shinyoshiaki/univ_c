/************************************************************/
/* Sortプログラムの動作確認                                 */
/*------------------------------------------                */
/*    大きさ100の配列にランダムに整数を生成し、             */
/*    その配列要素を別途用意された整列化プログラムを使って  */
/*    昇順に並べ替えて出力する。                            */
/************************************************************/

#include <stdio.h>
#include <stdlib.h>   /* 乱数発生のライブラリ関数を使うため */

#define  SIZE   100
#define  WIDTH  10

void set_an_array_random(int a[], int size);
void pretty_print(int a[], int size);
void sort(int a[], int size);
void sort_method(char *method_name);

int main(void)
{
  int   a[SIZE], seed;
  char  sort_name[40];

  printf("Input a random seed (0 - %d):  ", RAND_MAX);
  scanf("%d", &seed);
  srand(seed);

  set_an_array_random(a, SIZE);
  printf("\nbefore sorting:\n");
  pretty_print(a, SIZE);

  sort(a, SIZE);
  sort_method(sort_name);       /* Sortモジュールに         */
                                /* 何のアルゴリズムか尋ねる */
  printf("\nafter sorting (%s):\n", sort_name);
  pretty_print(a, SIZE);
  return 0;
}

/*----------------------------------------------------------*/
/* 引数で与えられた配列の各要素をランダムに設定する         */
/*------------------------------------------                */
/* (仮引数) a    : int型配列                                */
/*          size : int型配列 a の大きさ                     */
/* (関数値)  :  なし                                        */
/* (機能) :  配列要素 a[0]〜a[size-1] に 0〜999 の間の乱数  */
/*           を設定する。                                   */
/*----------------------------------------------------------*/
void set_an_array_random(int a[], int size)
{
  int i;

  for (i=0; i<size; ++i)
    a[i] = rand() % 1000;
}

/*----------------------------------------------------------*/
/* 引数で与えられた配列の要素を順番に全て出力する           */
/*------------------------------------------                */
/* (仮引数) a    : int型配列                                */
/*          size : int型配列 a の大きさ                     */
/* (関数値)  :  なし                                        */
/* (機能) :  配列要素 a[0]〜a[size-1] の値を順番に全て出力  */
/*           する。但し、各々の値は横幅7カラムのフィールド  */
/*           に出力することにし、また、1行にWIDTH個の要素   */
/*           を出力する。                                   */
/*----------------------------------------------------------*/
void pretty_print(int a[], int size)
{
  int i, count=1;

  for (i=0; i<size; ++i, ++count) {
    printf("%7d", a[i]);
    if (count >= WIDTH) {
      printf("\n");
      count = 0;
    }
  }
  if (count > 1)
    printf("\n");
}
