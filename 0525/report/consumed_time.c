/************************************************************/
/* 計算時間を計るための汎用モジュール                       */
/*------------------------------------------                */
/*    外部へのサービスを行うために、次の２つの関数がこの    */
/*    モジュールの中に用意されている。                      */
/*    (1)start_timekeeper ... 時間測定を開始させる関数      */
/*    (2)consumed_time ... 以前に関数 start_timekeeper また */
/*           は consumed_time が呼ばれた時点から現在までに  */
/*           消費したプロセッサ時間(秒)とカレンダー時間(秒) */
/*           の組み(構造体)を返す関数                       */
/************************************************************/

#include <time.h>

typedef struct {
   clock_t  clock;
   time_t   time;
} Time;

static Time  previous, current; /* 静的外部変数 ==>ファイル */
                                /*         外からは見えない */
typedef struct {
   double  process_time;
   double  real_time;
} Second;

/*----------------------------------------------------------*/
/* 計算時間を測定する際の開始時点を(静的外部変数に)記録する */
/*----------------------------------------------------------*/
void start_timekeeper(void)
{
  previous.clock = clock();
  previous.time  = time(NULL);
}

/*----------------------------------------------------------*/
/* 以前に start_timekeeper() または consumed_time() が      */
/* 呼ばれた時点から現在までに消費したプロセッサ時間、       */
/* とカレンダー時間(時刻)、の組み(構造体)を返す。           */
/*----------------------------------------------------------*/
Second consumed_time(void)
{
  Second  consumed;

  current.clock = clock();
  current.time  = time(NULL);
  consumed.process_time = (current.clock - previous.clock)
                                  / (double) CLOCKS_PER_SEC;
  consumed.real_time = difftime(current.time, previous.time);
  previous = current;
  return consumed;
}
