/************************************************************/
/* Quicksortモジュール                                     */
/*------------------------------------------                */
/*    外部へのサービスを行うために、次の２つの関数がこの    */
/*    モジュールの中に用意されている。                      */
/*    (1)整列化アルゴリズムの名前を答える関数 sort_method,  */
/*    (2)配列要素をQuicksortアルゴリズムで                 */
/*                   小さい順に並べ替える関数 sort          */
/************************************************************/

#include <stdio.h>

static void quicksort(int x[], int from, int to);
static int partition(int x[], int from, int to);

/*----------------------------------------------------------*/
/* 整列化アルゴリズムの名前を答える                         */
/*------------------------------------------                */
/* (引数) method_name : 出力用。このchar型配列に方式の名前  */
/*                      を(文字列として)入れて返す。配列の  */
/*                      大きさは 11 文字分必要。            */
/*----------------------------------------------------------*/
void sort_method(char *method_name)
{
    sprintf(method_name, "Quicksort");
}

/*----------------------------------------------------------*/
/* 配列要素を小さい順に並べ替える ( quicksort)             */
/*------------------------------------------                */
/* (仮引数) a    : int型配列                                */
/*          size : int型配列 a の大きさ                     */
/* (関数値)  :  なし                                        */
/* (機能) :  quicksortアルゴリズムを使って、配列要素       */
/*                a[0],a[1],a[2], ..., a[size-1]            */
/*           を値の小さい順に並べ替える。                   */
/*----------------------------------------------------------*/
void sort(int a[], int size)
{
    quicksort(a, 0, size - 1);
}

static void quicksort(int x[], int from, int to)
{
    int pivot_sub;

    if (from < to)
    {
        pivot_sub = partition(x, from, to);
        quicksort(x, from, pivot_sub - 1);
        quicksort(x, pivot_sub + 1, to);
    }
}

static int partition(int x[], int from, int to)
{
    int pivot;
    pivot = x[from];
    while (1)
    {
        for (; from < to && x[to] > pivot; --to)
            ;
        if (from == to)
        {
            x[from] = pivot;
            return from;
        }
        x[from++] = x[to];
        for (; from < to && x[from] <= pivot; ++from)
            ;
        if (from == to)
        {
            x[to] = pivot;
            return to;
        }
        x[to--] = x[from];
    }
}
