/***************************************************************/
/*                                                            */
/* ２分木上に識別子(と整数)を辞書順に登録し、最後に２分木を   */
/* 中間順に走査することによって蓄えられたデータを辞書順に出力 */
/*                                                            */
/**************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define  MAXLENGTH  100     /* 識別子の最大の長さ */

#define Is_empty(btree)  ((btree) == NULL)

typedef char *String;

typedef  struct btree_item  *Btree;
typedef struct btree_item {
    String id;
    int    num;
    Btree  left_child, right_child;
} Btree_item;

Btree add_item(Btree tree, String id, int num);
void inorder_traverse_and_print_free(Btree);

int main(void)
{
  Btree root;
  char  identifier[MAXLENGTH+1];
  int   count, num;

  root = NULL;
  while ((count=scanf("%100s %d", identifier, &num)) == 2)
    root = add_item(root, identifier, num);

  if (count != EOF) {
    printf("Input Error!\n");
    exit(EXIT_FAILURE);
  }

  printf("番号       整数  識別子\n");
  inorder_traverse_and_print_free(root);
  return 0;
}

/*--------------------------------------------------------------*/
/* ２分木上に識別子と整数の組を追加                             */
/*--------------------------------------------------------------*/
/* (仮引数) tree : データの登録先となる２分木へのポインタ       */
/*          id   : 文字列データへのポインタ                     */
/*          num  : 整数                                         */
/* (関数値) : データ登録後の２分木へのポインタ                  */
/* (機能)   : 識別子と整数の組を1つの項目として、それらが       */
/*                左の子とその子孫の識別子 <= 親の識別子        */
/*                親の識別子     <  右の子とその子孫の識別子    */
/*            を常に満たすように２分木状に保存されており、      */
/*            tree がその２分木の根を指し示すと仮定する。       */
/*            この仮定の下で、id の指し示す識別子と整数 num の  */
/*            組を新しい項目として（辞書順を保つ様に）２分木に  */
/*            挿入して、出来た２分木へのポインタを返す。        */
/*--------------------------------------------------------------*/
Btree add_item(Btree tree, String id, int num)
{
  Btree_item *new_item;

  if (Is_empty(tree)) { /*新しいitemを作り、そこへのポインタを返す*/
    new_item = (Btree_item *) malloc(sizeof(Btree_item));
    if (new_item == NULL) {
      printf("*** fail in memory allocation ***");
      exit(EXIT_FAILURE);
    }
    new_item->id = (String) malloc(strlen(id)+1);   /*idを入れる領*/
    if (new_item->id == NULL) {                     /*域を確保し、*/
      printf("*** fail in memory allocation ***");  /*そこへのポイ*/
      exit(EXIT_FAILURE);                           /*ンタを代入  */
    }
    strcpy(new_item->id, id);                 /* 新領域にidを複写 */
    new_item->num         = num;
    new_item->left_child  = NULL;
    new_item->right_child = NULL;
    return new_item;
  }else if (strcmp(id, tree->id) <= 0) { /* 左部分木へ挿入 */
    tree->left_child = add_item(tree->left_child, id, num);
    return tree;
  }else {                                /* 右部分木へ挿入 */
    tree->right_child = add_item(tree->right_child, id, num);
    return tree;
  }
}

/*--------------------------------------------------------------*/
/* ２分木の中味を辞書順に出力, 同時に２分木のメモリを解放       */
/*--------------------------------------------------------------*/
/* (仮引数) ptr : ２分木へのポインタ                            */
/* (関数値) : なし                                              */
/* (機能)   : 識別子と整数の組を1つの項目として、それらが       */
/*                左の子とその子孫の識別子 <= 親の識別子        */
/*                親の識別子     <  右の子とその子孫の識別子    */
/*            を常に満たすように２分木状に保存されており、ptr   */
/*            がその２分木の根を指し示すと仮定する。この仮定の  */
/*            下で、２分木を中間順に走査することによって、２分  */
/*            木に蓄えられたデータを                            */
/*                  番号  整数  識別子                          */
/*            という書式で出力する。                            */
/*            また、同時に２分木のメモリを解放する。            */
/*--------------------------------------------------------------*/
void inorder_traverse_and_print_free(Btree ptr)
{
  static int n=1;

  if (ptr != NULL) {
    inorder_traverse_and_print_free(ptr->left_child);
    printf("%4d%11d  %s\n", n++, ptr->num, ptr->id);
    inorder_traverse_and_print_free(ptr->right_child);
    free(ptr->id);
    free(ptr);
  }
} 
