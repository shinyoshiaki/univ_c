/* プログラミング AI( 金曜 3 〜 4 限 )*/
/* 工学科 情報工学科 */
/*学籍番号 T18I917F 氏名 沈 嘉秋 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define MAXLENGTH 100
#define Is_empty(list) ((list) == NULL)

typedef char *String;
typedef struct Student *List;
typedef struct Student //学生の構造体データ
{
    String id;
    int eng;
    int math;
    int jap;
    int total;
    List next;
} List_item;

void add_item(List *ptr_ptr, String id, int eng, int math, int jap, int total);
void listprint(List);

int get_length(List p);                          //線形リストの長さを取得
double deviation(int *arr, int length, int ave); //標準偏差を取得
void bubble_sort(struct Student *p);             //線形リストのバブルソート

int main(void)
{
    List head;
    String id;
    int count, eng, math, jap, total;
    head = NULL;
    while ((count = scanf("%100s %d %d %d", id, &eng, &math, &jap)) == 4)
    {
        total = eng + math + jap;
        add_item(&head, id, eng, math, jap, total);
    }
    if (count != EOF)
        exit(EXIT_FAILURE);

    listprint(head);
    return 0;
}

void add_item(List *ptr_ptr, String id, int eng, int math, int jap, int total)
{
    List_item *new_item;
    while (!Is_empty(*ptr_ptr) && strcmp(id, (*ptr_ptr)->id) > 0)
        ptr_ptr = &((*ptr_ptr)->next);
    new_item = (List_item *)malloc(sizeof(List_item));
    if (new_item == NULL)
        exit(EXIT_FAILURE);
    new_item->id = (String)malloc(strlen(id) + 1);
    if (new_item->id == NULL)
        exit(EXIT_FAILURE);
    strcpy(new_item->id, id);

    new_item->eng = eng;
    new_item->math = math;
    new_item->jap = jap;
    new_item->next = *ptr_ptr;
    new_item->total = total;
    *ptr_ptr = new_item;
}

int get_length(List p) //線形リストの長さを取得
{
    int data_count = 0;
    while (p != NULL)
    {
        data_count++;
        p = p->next;
    }
    return data_count;
}

void bubble_sort(struct Student *p) //線形リストのバブルソート
{
    struct Student *head;
    struct Student *back;
    double tmp = 0;
    double sum = 0;
    int i, j, k;
    int data_count = get_length(p);

    head = p;

    for (i = 0; i < data_count - 1; i++)
    {
        for (j = data_count - 1; j > i; j--)
        {
            p = head;
            for (k = 0, p = head; k < j - 1; k++)
            {
                p = p->next;
            }
            back = p;
            p = p->next;
            if (p->total > back->total)
            {
                tmp = p->total;
                p->total = back->total;
                back->total = tmp;
            }
        }
    }
}

double deviation(int *arr, int length, int ave) //標準偏差を取得
{
    int i, hensa, bunsan;
    for (i = 0; i < length; i++)
    {
        hensa += pow(arr[i] - ave, 2);
    }
    bunsan = hensa / length;

    return sqrt(bunsan);
}

void listprint(List ptr)
{
    int length = get_length(ptr);
    int i;
    int eng[MAXLENGTH], math[MAXLENGTH], jap[MAXLENGTH], total[MAXLENGTH];
    double eng_t, math_t, jap_t, total_t, eng_a, math_a, jap_a, total_a;
    printf("Id-No   Eng  Math   Jap   Total\n-----  ----  ----  ----   -----\n");
    bubble_sort(ptr);
    for (i = 0; !Is_empty(ptr); ptr = ptr->next, i++)
    {
        eng[i] = ptr->eng;
        math[i] = ptr->math;
        jap[i] = ptr->jap;
        total[i] = ptr->total;
        printf("%s  %4d  %4d  %4d    %4d\n", ptr->id, eng[i], math[i], jap[i], total[i]);
        eng_t += eng[i];
        math_t += math[i];
        jap_t += jap[i];
        total_t += total[i];
    }
    eng_a = eng_t / length;
    math_a = math_t / length;
    jap_a = jap_t / length;
    total_a = total_t / length;
    printf("-------------------------------\n");
    printf("  Ave  %4.1lf  %4.1lf  %4.1lf   %4.1lf\n",
           eng_a, math_a, jap_a, total_a); //平均を出力
    printf("  Dev  %4.1lf  %4.1lf  %4.1lf   %4.1lf\n",
           deviation(eng, length, eng_a), //標準偏差を出力
           deviation(math, length, math_a),
           deviation(jap, length, jap_a),
           deviation(total, length, total_a));
}
