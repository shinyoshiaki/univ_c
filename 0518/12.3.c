#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MAXLENGTH 100
#define Is_empty(list) ((list) == NULL)

typedef char *String;
typedef struct list_item *List;
typedef struct list_item
{
    String id;
    int num;
    List next;
} List_item;

void add_item(List *ptr_ptr, String id, int num);
void listprint(List);

int main(void)
{
    List head;
    char identifier[MAXLENGTH + 1];
    int count, num;
    head = NULL;
    while ((count = scanf("%100s %d", identifier, &num)) == 2)
        add_item(&head, identifier, num);
    if (count != EOF)
    {
        printf("Input Error!\n");
        exit(EXIT_FAILURE);
    }
    listprint(head);
    return 0;
}
/*--------------------------------------------------------------*/

void add_item(List *ptr_ptr, String id, int num)
{
    List_item *new_item;
    while (!Is_empty(*ptr_ptr) && strcmp(id, (*ptr_ptr)->id) > 0)
        ptr_ptr = &((*ptr_ptr)->next);
    /* 挿入場所を探す */
    new_item = (List_item *)malloc(sizeof(List_item));
    /* 新しい item 枠を作る */
    if (new_item == NULL)
    {
        printf("*** fail in memory allocation ***");
        exit(EXIT_FAILURE);
    }
    new_item->id = (String)malloc(strlen(id) + 1);
    /*id を入れる領 */
    if (new_item->id == NULL)
    {
        /* 域を確保し、 */
        printf("*** fail in memory allocation ***"); /* そこへのポイ */
        exit(EXIT_FAILURE);
        /* ンタを代入 */
    }
    strcpy(new_item->id, id);
    /* 新領域に id を複写 */
    new_item->num = num;
    new_item->next = *ptr_ptr;
    *ptr_ptr = new_item;
    /* ポインタの付け替え */
}

void listprint(List ptr)
{
    int n;
    printf(" 番号   整数 識別子 \n");
    for (n = 1; !Is_empty(ptr); ptr = ptr->next, ++n)
        printf("%4d%11d %s\n", n, ptr->num, ptr->id);
}