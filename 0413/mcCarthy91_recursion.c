#include "stdio.h"

int mccarthy91(int x);
int up = 0, down = 0;

int main()
{
    int x;
    scanf("%d", &x);

    printf("ans:%d\n", mccarthy91(x));
    return 0;
}

int mccarthy91(int x)
{
    if (x > 100)
    {
        printf("up %d %d\n", x, up++);
        return x - 10;
    }
    else
    {
        printf("down %d %d\n", x, down++);
        return mccarthy91(mccarthy91(x + 11));
    }
}