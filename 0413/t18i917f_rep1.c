/* プログラミングAI(金曜3〜4限)      */
/* 工学科 情報工学科 */
/*  学籍番号 T18I917F 氏名 沈 嘉秋 */

#include "stdio.h"

/*
整数データxを読み込み、
そのxに対するMcCarthyの91関数の値を出力するCプログラム
*/

int mccarthy91(int x);

int main()
{
    int x;
    printf("It will compute McCarthy's 91 function of an input integer.\ninput an integer: ");
    scanf("%d", &x);

    printf("maccarthy91(%d) = %d\n", x, mccarthy91(x));
    return 0;
}

/*
McCarthyの91関数(非再帰版)
(仮引数)x：任意の整数
(戻り値) ：引数xに対するMcCarthyの91関数の値
*/
int mccarthy91(int x)
{
    int end = 1;
    while (1)
    {
        if (x > 100)
        {
            x -= 10;
            end--;
        }
        else
        {
            x += 11;
            end++;
        }
        if (end == 0)
            break;
    }
    return x;
}