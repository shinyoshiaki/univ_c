#include "stdio.h"
#define LIMIT 7

void set_array_to_keep_integer(int a[], int size, int init_value);
void divide_array_value_by_integer(int a[], int size, int divisor);
void add_integer_to_array(int a[], int size, int value);
void print_array_value(int a[], int size, char *padding);

int main(void)
{
    int e[251], k;

    set_array_to_keep_integer(e, 251, 1);

    for (k = 450; k > 0; --k)
    {
        divide_array_value_by_integer(e, 251, k);
        add_integer_to_array(e, 251, 1);
    }

    printf("e =");
    print_array_value(e, 251, "   ");
    return 0;
}

void set_array_to_keep_integer(int a[], int size, int init_value)
{
    int i;

    a[0] = init_value;
    for (i = 1; i < size; ++i)
        a[i] = 0;
}

void divide_array_value_by_integer(int a[], int size, int divisor)
{
    int i;

    for (i = 0; i < size - 1; ++i)
    {
        a[i + 1] += (a[i] % divisor) * 10000;
        a[i] /= divisor;
    }
    a[size - 1] /= divisor;
}

void add_integer_to_array(int a[], int size, int addend)
{
    a[0] += addend;
}

void print_array_value(int a[], int size, char *padding)
{
    int i, count;

    printf("%2d.", a[0]);
    count = 1;
    for (i = 1; i < size; i += 2, ++count)
    {
        printf("%04d%04d ", a[i], a[i + 1]);
        if (count >= LIMIT)
        {
            printf("\n%s  ", padding);
            count = 0;
        }
    }
    printf("\n");
}