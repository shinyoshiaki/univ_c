#include <stdio.h>
int main(void)
{
    int mx[160] = {0};
    int i;     // 桁
    int j;     // mx[k]の最高位の桁
    int k;     // 階乗
    int p;     // 表示桁位置
    int carry; // 桁上がり

    mx[1] = 1, j = 1;
    for (k = 1; k <= 100; k++)
    {              // 1!～100!の計算
        carry = 0; // 最下位桁に対する桁上がりはない
        for (i = 1; i <= j + 1; i++)
        {
            mx[i] = mx[i] * k + carry; // k倍し、下位の桁からの桁上がりを加える
            // if(mx[i] >= 10){
            // carry = mx[i]/10;// 上位桁への桁上がりを控える
            // mx[i] %= 10; // その桁の数が決まる
            // if((i+1)>j) j++;// 前の階乗の最高位を超える場合
            // }
            // else{
            // carry=0;
            // }
            // ■上の8行を改良した
            carry = mx[i] / 10; // 上位桁への桁上がりを控える
            if (carry)
            {
                mx[i] %= 10; // その桁の数が決まる
                if ((i + 1) > j)
                    j++; // 前の階乗の最高位を超える場合
            }
            // ■改良終り
        }
        printf("%3d: ", k);
        p = j;
        while (p > 0)
            printf("%d", mx[p--]);
        putchar('\n');
    }
    return 0;
}
