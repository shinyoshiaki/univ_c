#include "stdio.h"

#define N 52

void display();
int factorial(int n);

int main()
{
    int result[N], i;
    for (i = 1; i < N + 1; i++)
    {
        result[i - 1] = factorial(i);
    }
    display(result);
    return 0;
}

int factorial(int n)
{
    if (n > 0)
    {
        return n * factorial(n - 1);
    }
    else
    {
        return 1;
    }
}

void display(int result[])
{
    int i;
    for (i = 1; i < N + 1; i++)
    {
        printf("%2d! = %d\n", i, result[i - 1]);
    }
}
