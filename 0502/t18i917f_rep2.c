/* プログラミング AI( 金曜 3 〜 4 限 )*/
/* 工学科 情報工学科 */
/*学籍番号 T18I917F 氏名 沈 嘉秋 */

#include <stdio.h>

void factorial(int n, int length);                  //階乗の計算
void display(int k, int p, int length, int nums[]); //階乗の計算結果を整形して表示

int main(void)
{
    factorial(52, 70);

    return 0;
}

void factorial(int n, int length)
{
    //引数n：乗数,引数length：表示する際の桁数
    int nums[100] = {0}; //計算結果
    int i, j, k;
    int carry; //桁上がり
    int p;     //桁の位置
    int tmp;

    nums[1] = 1;
    j = 1;
    for (k = 1, carry = 0; k <= n; k++)
    {
        for (i = 1; i <= j + 1; i++)
        { //筆算のようなイメージで処理する
            nums[i] = nums[i] * k + carry;
            carry = nums[i] / 10;
            if (carry)
            {
                nums[i] %= 10;
                if (i + 1 > j)
                    j++;
            }
        }
        display(k, j, length, nums);
    }
}

void display(int k, int p, int length, int nums[])
{
    //引数k：乗数,引数p：桁の位置,引数length：表示する際の桁数,引数nums[]：計算結果
    int tmp;
    printf("%2d! = ", k);

    for (tmp = length - p; tmp > 0; tmp--)
        printf(" ");

    while (p > 0)
        printf("%d", nums[p--]);
    printf("\n");
}