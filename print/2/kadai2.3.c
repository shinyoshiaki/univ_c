#include <stdio.h>

#define cube(x) (x * x * x)

int main()
{
    int i;
    double x;

    for (i = 1; i <= 5; i++)
    {
        x = (double)i;
        printf("%4.1f   %g\n", x, 1 / cube(x));
    }
}