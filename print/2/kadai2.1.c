#include <stdio.h>

typedef struct node *Tree;
typedef struct node
{
    int data;
    Tree left_subtree;
    Tree right_subtree;
} Node;

int size(Tree t);
int num = 0;

int main()
{
    Node a, b, c, d;
    a.left_subtree = &b;
    a.right_subtree = NULL;
    b.left_subtree = &c;
    b.right_subtree = &d;
    c.left_subtree = c.right_subtree = NULL;
    d.left_subtree = d.right_subtree = NULL;
    printf("size(&a)=%d\n", size(&a));
    printf("size(&b)=%d\n", size(&b));
    printf("size(&c)=%d\n", size(&c));
    printf("size(&d)=%d\n", size(&d));
    printf("size(NULL)=%d\n", size(NULL));
}

int size(Tree t)
{
    int m = 0, l, r;
    if (t == NULL)
        return 0;

    r = size(t->right_subtree);
    l = size(t->left_subtree);
    m++;

    return m + l + r;
}