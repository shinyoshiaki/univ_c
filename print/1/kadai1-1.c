#include <stdio.h>
int main(void)
{
    int i;
    char a[6] = "Emacs", *x = a;
    printf("(1)1+1/2*4 => %d\n", 1 + 1 / 2 * 4);
    printf("(2)1+1.0/2*4 => %3.1f\n", 1 + 1.0 / 2 * 4);
    printf("(3)1+1/2*4.0 => %3.1f\n", 1 + 1 / 2 * 4.0);
    printf("(4)%s\n", a);
    printf("(5)%c%c%c\n", a[0], a[1], a[2]);
    printf("(6)%4d%4d\n", a[0], a[1]);
    printf("(7)%s\n", x + 1);
    printf("(8)%c\n", *x + 1);
    printf("(9)%c\n", ("bash" + 1)[0]);
    printf("(10)scanf()=%d\n", scanf("%d", &i));

    return 0;
}