#include <stdio.h>
#include <math.h>

double heikin(int n, double x[])
{
    int i;
    double sum = 0;
    for (i = 0; i < n; i++)
        sum += x[i];

    return sum / n;
}

double bunsan(int n, double x[])
{
    int i;
    double ju = 0;
    for (i = 0; i < n; i++)
        ju += pow(x[i], 2);

    return ju / n - pow(heikin(n, x), 2);
}

int main()
{
    int n, i;
    double x[100];
    printf("input n\n");
    scanf("%d", &n);
    printf("input arr\n");
    for (i = 0; i < n; i++)
    {
        scanf("%lf", &x[i]);
    }
    printf("heikin : %f\n", heikin(n, x));
    printf("bunsan : %f\n", bunsan(n, x));
    return 0;
}
