#include <stdio.h>
void f1(int p, int q);
void f2(int *p, int *q);
int g(int val);

int main(void)
{
    int a, b;
    a = 1;
    b = 2;
    f1(a, b);
    printf("(1)a=%d, b=%d\n", a, b);
    a = 1;
    b = 2;
    f2(&a, &b);
    printf("(2)a=%d, b=%d\n", a, b);

    printf("(3)g(3)=%d\n", g(3));
    printf("(4)g(5)=%d\n", g(5));
    printf("(5)g(5)=%d\n", g(5));
    printf("(6)g(5)=%d\n", g(5));
    return 0;
}
void f1(int p, int q)
{
    int tmp;
    tmp = p;
    p = q;
    q = tmp;
}

void f2(int *p, int *q)
{
    int tmp;
    tmp = *p;
    *p = *q;
    *q = tmp;
}

int g(int val)
{
    static int x[2] = {1, 2};
    int ans;
    ans = x[0];
    x[0] = x[1];
    x[1] = val;
    return ans;
}